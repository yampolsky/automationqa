package com.welcome;


public class Hello {
    private String name = "";
    public void setupName(String name) {
        this.name = name;
    }
    public void welcome(){
        System.out.print("Hello " + name);
    }
    public void byebye(){
        System.out.print("Bye-bye " + name);
    }

}

